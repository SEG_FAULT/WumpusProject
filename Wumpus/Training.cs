﻿using System;
using System.Collections.Generic;
using System.IO;
using Wumpus.Agents;
using Wumpus.Genetics;
using Wumpus.NeuralNet;

namespace Wumpus
{
    public class Training
    {
        private List<Individual> m_population = new List<Individual>();
        private List<Individual> m_lastGeneration = new List<Individual>();
        private List<Individual> m_currentGeneration = new List<Individual>();
        
        private static readonly List<int> m_neuronsPerLayer = new List<int> {29, 100, 100, 4};
        
        int m_generationID;
        private readonly string m_log = @"lastweights" + DateTimeOffset.Now.ToUnixTimeSeconds() + ".txt";

        private const int m_gen_toMutate      = 10;
        private const int m_gen_toMerge       = 10;
        private const int m_gen_toMutateMerge = 10;
        private const int m_gen_lastBest      = 10;
        private const int m_gen_allBest       = 10;
        private const int m_gen_newRandom     = 70;
        private const int m_generationSize = m_gen_toMutate + m_gen_toMerge + m_gen_toMutateMerge 
                                              + m_gen_lastBest + m_gen_allBest + m_gen_newRandom;



        public void train()
        {
            List<int[]> maps = new List<int[]>
            {
                new[] {20, 20, 5},
                new[] {20, 20, 5},
                new[] {20, 20, 5},
                new[] {20, 20, 5},
                new[] {20, 20, 5},
                new[] {20, 20, 5},
                new[] {20, 20, 5},
                new[] {20, 20, 5},
                new[] {20, 20, 5}
            };
            train(maps, 10, 5000);
        }
        /*
         *
         * Params:
         *     newMapTurnover: Generate new maps every X generation
         *     
         */
        public void train(List<int[]> mapsConf, int newMapTurnover, int maxGeneration)
        {   
            // first generation
            for(int i = 0; i < m_generationSize; ++i)
                m_currentGeneration.Add(new Individual(new FFNN(m_neuronsPerLayer)));
            
            List<Game.Map> maps = new List<Game.Map>();
            
            for(m_generationID = 0; m_generationID < maxGeneration; ++m_generationID)
            {
                if(m_generationID % newMapTurnover == 0)
                {
                    maps.Clear();
                    foreach(int[] map in mapsConf)
                        maps.Add(Game.Map.generateMap(map[0], map[1], map[2]));
                }

                Console.Write("Generation : {0}\t", m_generationID);
                for(int i = 0; i < maps.Count; ++i)
                    runGeneration(maps[i]);

                endGeneration();
                if((m_generationID + 1) % 10 == 0)
                {
                    Console.Beep(450, 1000);
                    Console.ReadKey();
                    Testing t = new Testing();
                    List<Agent> a = new List<Agent>();
                    a.Add(m_population[0].generateAgent(250));
                    t.tests(a, maps);
                }

                toConsole();
                //toFile();
                
                m_currentGeneration = getNextGeneration();

            }
        }

        private void toFile()
        {
            List<double> a =  m_population[0].getBrain().getDNA();
            Console.WriteLine(a.Count);
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(m_log, true))
            {
                file.Write(m_population[0].getScore());
                file.Write('\t');
                foreach(double d in a)
                {
                    file.Write(d);
                    file.Write('\t');
                }
                file.Write('\n');
            }
            a.RemoveRange(10, a.Count - 10);
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(m_log+"-short", true))
            {
                file.Write(m_population[0].getScore());
                file.Write('\t');
                foreach(double d in a)
                {
                    file.Write(d);
                    file.Write(", ");
                }
                file.Write('\n');
            }
        }

        private void toConsole()
        {
            for(int i = 0; i < 8; ++i)
                Console.Write("{1}\t", i, m_currentGeneration[i].getScore());
            Console.Write("\t");
            int c = 0;
            for(int i = 0, fscore = m_currentGeneration[0].getScore(); i < m_currentGeneration.Count && fscore == m_currentGeneration[i].getScore(); ++i)
                c = i;
            Console.WriteLine("{0} / {1}", c, m_currentGeneration.Count);
            /*for(int i = 0; i < 2; ++i)
                Console.Write("{1}\t", i, m_generation[m_generation.Count - i - 1].getScore());
            Console.Write("\n");*/
        }

        private void endGeneration()
        {
            m_currentGeneration.Sort();
            m_lastGeneration = m_currentGeneration;
            m_population.AddRange(m_lastGeneration);
            m_population.Sort();
            if(m_population.Count > 99)
                m_population.RemoveRange(99, m_population.Count - 99);
        }

        private void runGeneration(Game.Map map)
        {
            for(int i = 0; i < m_currentGeneration.Count; ++i)
            {
                Individual individual = m_currentGeneration[i];
                Agent agent = individual.generateAgent(250);
                agent.setMap(map);
                map.reset();
                map.setAgent(agent);

                while(!map.isResolved() && agent.isAlive())
                    individual.nextMove();
            }
        }

        private List<Individual> getNextGeneration()
        {
            List<Individual> result = new List<Individual>();
            List<Individual> tmp = m_population.GetRange(0, m_gen_allBest);

            for(int i = 0; i < m_gen_toMutate; ++i)
                result.Add(DNA.mutate(tmp[i], .5));
            
            tmp = m_lastGeneration.GetRange(0, m_gen_lastBest);
            for(int i = 0; i < m_gen_lastBest; ++i)
                result.Add(new Individual(tmp[i].getBrain()));

            tmp = m_population.GetRange(0, m_gen_allBest);
            for(int i = 0; i < m_gen_allBest; ++i)
                result.Add(new Individual(tmp[i].getBrain()));
            
            
            for(int i = 0; i < m_gen_newRandom; ++i)
                result.Add(new Individual(new FFNN(m_neuronsPerLayer)));
            return result;
        }
    }
}