﻿using System;
using System.Collections.Generic;
using Wumpus.Agents;

namespace Wumpus
{
    public abstract partial class Game
    {
        public class ConsoleDisplayer : Displayer
        {
            const int caseSizeX = 3;
            const int caseSizeY = 3;

            private static readonly Tile[] tilesData = initTiles();
            private const int tileWidth = 3;
            private const int tileHeight = 3;


            private static ConsoleColor[][] initTilesColor()
            {
                // 0 : Background ; 1 : Foreground
                ConsoleColor[][] result = new ConsoleColor[13][];
                result[(int)Tile.Type.PlayerAlive] = new[] {ConsoleColor.Blue, ConsoleColor.White};
                result[(int)Tile.Type.PlayerDead] = new[] {ConsoleColor.DarkBlue, ConsoleColor.White};
                result[(int)Tile.Type.Well] = new[] {ConsoleColor.Black, ConsoleColor.White};
                result[(int)Tile.Type.Wumpus] = new[] {ConsoleColor.DarkGreen, ConsoleColor.White};
                result[(int)Tile.Type.Gold] = new[] {ConsoleColor.DarkYellow, ConsoleColor.Black};
                result[(int)Tile.Type.WumpusDead] = new[] {ConsoleColor.DarkRed, ConsoleColor.White};
                return result;
            }

            private static Tile[] initTiles()
            {
                Tile[] result = new Tile[13];
                result[(int)Tile.Type.PlayerAlive] = new Tile()
                    .setData(
                        "^ ^" +
                        " - " +
                        "\\ /")
                    .setForeground(ConsoleColor.White)
                    .setBackground(ConsoleColor.Blue);
                result[(int)Tile.Type.PlayerDead] = new Tile()
                    .setData(
                        "X X" +
                        " . " +
                        ". .")
                    .setForeground(ConsoleColor.White)
                    .setBackground(ConsoleColor.DarkBlue);
                result[(int)Tile.Type.Empty] = new Tile()
                    .setData(
                        "   " +
                        "   " +
                        "   ");
                result[(int)Tile.Type.Wall] = new Tile()
                    .setData(
                        "###" +
                        "###" +
                        "###")
                    .setDataDead(
                        "###" +
                        "###" +
                        "###")
                    .setForeground(ConsoleColor.White)
                    .setBackground(ConsoleColor.DarkGray)
                    .setForegroundDead(ConsoleColor.White)
                    .setBackgroundDead(ConsoleColor.DarkRed);
                result[(int)Tile.Type.Well] = new Tile()
                    .setData(
                        "/ \\" +
                        "   " +
                        "\\ /")
                    .setForeground(ConsoleColor.White)
                    .setBackground(ConsoleColor.Black);
                result[(int)Tile.Type.Wind] = new Tile()
                    .setData(
                        "   " +
                        " ~ " +
                        "   ");
                result[(int)Tile.Type.Smell] = new Tile()
                    .setData(
                        "~ ~" +
                        "   " +
                        "~ ~");
                result[(int)Tile.Type.Wumpus] = new Tile()
                    .setData(
                        "o o" +
                        " | " +
                        "/ \\")
                    .setForeground(ConsoleColor.White)
                    .setBackground(ConsoleColor.DarkGreen);
                result[(int)Tile.Type.Gold] = new Tile()
                    .setData(
                        " G " +
                        "G G" +
                        " G ")
                    .setForeground(ConsoleColor.White)
                    .setBackground(ConsoleColor.DarkYellow);
                result[(int)Tile.Type.Start] = new Tile()
                    .setData(
                        "SSS" +
                        "SSS" +
                        "SSS");
                result[(int)Tile.Type.WumpusDead] = new Tile()
                    .setData(
                        "X X" +
                        " - " +
                        "/ \\")
                    .setForeground(ConsoleColor.White)
                    .setBackground(ConsoleColor.DarkRed);
                result[(int)Tile.Type.Even] = new Tile()
                    .setData(
                        "   " +
                        "   " +
                        "   ")
                    .setDataDead(
                        "   " +
                        "   " +
                        "   ")
                    .setForeground(ConsoleColor.White)
                    .setBackground(ConsoleColor.DarkGray)
                    .setForegroundDead(ConsoleColor.White)
                    .setBackgroundDead(ConsoleColor.DarkRed);
                result[(int)Tile.Type.Odd] = new Tile()
                    .setData(
                        "   " +
                        "   " +
                        "   ")
                    .setDataDead(
                        "   " +
                        "   " +
                        "   ")
                    .setForeground(ConsoleColor.White)
                    .setBackground(ConsoleColor.Gray)
                    .setForegroundDead(ConsoleColor.White)
                    .setBackgroundDead(ConsoleColor.Red);
                return result;
            }

            private static string[] initMapTiles()
            {
                string[] result = new string[9];

                return result;
            }


            private void initDisplay()
            {
                Console.WindowWidth = caseSizeX * m_width + 50;
                Console.WindowHeight = caseSizeY * m_height + 10;
            }

            private void endDisplay()
            {
                Console.ResetColor();
                Console.SetCursorPosition(0, caseSizeY * m_height);
            }

            public override void display()
            {
                if(Environment.GetEnvironmentVariable("FOG") == "true" && m_agent.isAlive())
                {
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.Clear();
                    return;
                }

                initDisplay();
                for(int y = 0; y < m_height; ++y)
                for(int x = 0; x < m_width; ++x)
                    displayCase(x, y);
                endDisplay();
            }

            private bool caseDiscovered(int x, int y)
            {
                if(Environment.GetEnvironmentVariable("FOG") == "false" || !m_agent.isAlive())
                    return true;
                try
                {
                    AgentDiscover d = (AgentDiscover)m_agent;
                    Position pos = new Position(x - 1, m_height - y - 2);
                    return d.m_discover.ContainsKey(pos)
                           && d.m_discover[pos].visitCount > 0;
                }
                catch
                {
                    return true;
                }
            }

            public override void update()
            {
                initDisplay();
                Position agent = m_agent.getPosition();
                for(int y = agent.y - 1; y < agent.y + 2; ++y)
                for(int x = agent.x - 1; x < agent.x + 2; ++x)
                    displayCase(x, y);
                for(int y = 0; y < m_height; ++y)
                {
                    displayCase(0, y);
                    displayCase(m_width - 1, y);
                }

                for(int x = 0; x < m_width; ++x)
                {
                    displayCase(x, 0);
                    displayCase(x, m_height - 1);
                }

                endDisplay();
            }

            private void displayCase(int x, int y)
            {
                if(!checkPosition(x, y) || !caseDiscovered(x, y))
                    return;
                Tile.Type[] tiles = getTiles(x, y);
                for(int i = 0; i < tiles.Length; ++i)
                    tilesData[(int)tiles[i]].display(x, y, m_agent.isAlive(), i == 0);
            }

            private bool checkPosition(int x, int y)
            {
                return x >= 0 && x < m_width
                              && y >= 0 && y < m_height;
            }

            private Tile.Type[] getTiles(int x, int y)
            {
                List<Tile.Type> result = new List<Tile.Type>();
                if(State.hasState(m_map[x, y], Case.Gold))
                    result.Add(Tile.Type.Gold);
                else if(m_agent.getPosition() == new Position(x, y))
                    result.Add(m_agent.isAlive()
                        ? Tile.Type.PlayerAlive
                        : Tile.Type.PlayerDead);
                else if(State.hasState(m_map[x, y], Case.Well))
                    result.Add(Tile.Type.Well);
                else if(State.hasState(m_map[x, y], Case.Wumpus))
                    result.Add(Tile.Type.Wumpus);
                else if(State.hasState(m_map[x, y], Case.WumpusDead))
                    result.Add(Tile.Type.WumpusDead);
                else if(State.hasState(m_map[x, y], Case.Wall))
                    result.Add(Tile.Type.Wall);
                else
                    result.Add((x + y) % 2 == 0 ? Tile.Type.Even : Tile.Type.Odd);


                if(State.hasState(m_map[x, y], Case.Smell))
                    result.Add(Tile.Type.Smell);
                if(State.hasState(m_map[x, y], Case.Wind))
                    result.Add(Tile.Type.Wind);

                return result.ToArray();
            }

            private class Tile
            {
                public enum Type
                {
                    PlayerAlive = 0,
                    PlayerDead,
                    Empty,
                    Wall,
                    Well,
                    Wind,
                    Smell,
                    Wumpus,
                    Gold,
                    Start,
                    WumpusDead,
                    Even,
                    Odd
                }

                private string[] m_data;
                private ConsoleColor[] m_background;
                private ConsoleColor[] m_foreground;
                private bool m_hasBackground;

                public Tile()
                {
                    m_data = new string[2];
                    m_foreground = new ConsoleColor[2];
                    m_background = new ConsoleColor[2];
                    m_hasBackground = false;
                }

                public Tile setData(string data)
                {
                    m_data[0] = data;
                    return this;
                }

                public Tile setDataDead(string data)
                {
                    m_data[1] = data;
                    return this;
                }

                public Tile setForeground(ConsoleColor data)
                {
                    m_foreground[0] = data;
                    return this;
                }

                public Tile setForegroundDead(ConsoleColor data)
                {
                    m_foreground[1] = data;
                    return this;
                }

                public Tile setBackground(ConsoleColor data)
                {
                    m_hasBackground = true;
                    m_background[0] = data;
                    return this;
                }

                public Tile setBackgroundDead(ConsoleColor data)
                {
                    m_hasBackground = true;
                    m_background[1] = data;
                    return this;
                }

                public string getData(bool agentAlive)
                {
                    return m_data[agentAlive || m_data[1] == null ? 0 : 1];
                }

                public ConsoleColor getBackgroundColor(bool agentAlive)
                {
                    return m_background[agentAlive || m_data[1] == null ? 0 : 1];
                }

                public ConsoleColor getForegroundColor(bool agentAlive)
                {
                    return m_foreground[agentAlive || m_data[1] == null ? 0 : 1];
                }

                private void setColors(bool agentAlive)
                {
                    Console.ForegroundColor = getForegroundColor(agentAlive);
                    if(m_hasBackground)
                        Console.BackgroundColor = getBackgroundColor(agentAlive);
                }

                public void display(int x, int y, bool agentAlive, bool displaySpaces = false)
                {
                    string tileString = getData(agentAlive);
                    for(int i = 0; i < tileWidth; ++i)
                    {
                        for(int j = 0; j < tileHeight; ++j)
                        {
                            int index = i + j * tileWidth;
                            if(tileString[index] != ' ' || displaySpaces)
                            {
                                setColors(agentAlive);
                                Console.SetCursorPosition(x * tileWidth + i, y * tileHeight + j);
                                Console.Write(tileString[index]);
                            }
                        }
                    }
                }
            }
        }
    }
}