﻿using System;

namespace Wumpus
{
    public abstract partial class Game
    {
        public partial class Map
        {
            private static Random m_random = null;
            
            public static Map generateMap(int lines, int columns, int wellCount)
            {
                if(m_random == null)
                    m_random = new Random();
                
                Map result = new Map(columns, lines);
                result.m_name = lines + "x" + columns + "x" + wellCount;
                Position pos = new Position(1, lines - 2);
                result.m_startPosition = pos;
                result.setState(pos, Case.Start);
                pos.x += 1;
                result.setState(pos, Case.Start);
                pos.x -= 1;
                pos.y -= 1;
                result.setState(pos, Case.Start);
                result = addWall(result);
                result = generateMap(result, wellCount);
                //Console.WriteLine("Map generated");
                return result;
            }
    
            private static Map generateMap(Map map, int wellCount)
            {
                Position pos;
    
                // Add gold case
                map.setState(findEmptyPlace(map), Case.Gold);
                
                // Add Wumpus case
                pos = findEmptyPlace(map);
                map.m_wumpusPosition = pos;
                map.setState(pos, Case.Wumpus);
                map.addToAdjacent(pos, Case.Smell);
    
                // Add well cases
                for(int i = 0; i < wellCount; ++i)
                {
                    pos = findEmptyPlace(map);
                    map.setState(pos, Case.Well);
                    map.addToAdjacent(pos, Case.Wind);
                }
                
                return map;
            }
            private static Map addWall(Map map)
            {
                for(int i = 0; i < map.m_width; ++i)
                {
                    map.setState(new Position(i, 0), Case.Wall);
                    map.setState(new Position(i, map.m_height - 1), Case.Wall);
                }
                for(int i = 1; i < map.m_height - 1; ++i)
                {
                    map.setState(new Position(0, i), Case.Wall);
                    map.setState(new Position(map.m_width - 1, i), Case.Wall);
                }
                
                return map;
            }
    
            private void addToAdjacent(Position pos, Case flag)
            {
                if(!checkPosition(pos))
                    return;
                
                if(pos.x > 0)
                    addState(new Position(pos.x - 1, pos.y), flag);
                if(pos.y > 0)
                    addState(new Position(pos.x, pos.y - 1), flag);
                if(pos.x < m_width - 1)
                    addState(new Position(pos.x + 1, pos.y), flag);
                if(pos.y < m_height - 1)
                    addState(new Position(pos.x, pos.y + 1), flag);
            }
            
            private static Position findEmptyPlace(Map map)
            {
                Position result;
                int iteration = 0;
                do
                {
                    result.x = m_random.Next(map.m_width);
                    result.y = m_random.Next(map.m_height);
                    ++iteration;
                } while(map.m_map[result.x, result.y] != 0 && iteration < map.m_map.Length);
                return iteration >= map.m_map.Length ? new Position(-1, -1) : result; // Or throw an Exception on max iteration
            }
        }
    }
}