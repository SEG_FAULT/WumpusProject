﻿using System;
using Wumpus.Agents;

namespace Wumpus
{
    public abstract partial class Game
    {
        [Flags]
        public enum Case : byte
        {
            Empty = 0x00,
            Wall = 0x01,
            Well = 0x02,
            Wind = 0x04,
            Smell = 0x08,
            Wumpus = 0x10,
            Gold = 0x20,
            Start = 0x40,
            WumpusDead = 0x80
        }
        
        public partial class Map : IAction
        {
            private readonly int m_width;
            private readonly int m_height;
            private readonly Case[,] m_map;
            private Position m_startPosition;
            private Position m_wumpusPosition;
            private string m_name;
            
            private bool m_resolved;
            private Agent m_agent;
            private Displayer m_displayer;

            private Map(int width, int height)
            {
                m_width = width;
                m_height = height;
                m_resolved = false;
                m_map = new Case[m_width, m_height];
            }

            
            // Getters/Setters
            public void setAgent(Agent agent)
            {
                m_agent = agent;
            }
            public void setDisplayer(Displayer displayer)
            {
                m_displayer = displayer;
                m_displayer.setMapData(m_map, m_width, m_height);
                m_displayer.setAgent(m_agent);
            }
            public void reset()
            {
                setState(m_wumpusPosition, Case.Wumpus);
                m_resolved = false;
            }

            public string getName()
            {
                return m_name;
            }
            public bool isResolved()
            {
                return m_resolved;
            }
            public Position getStartingPosition()
            {
                return m_startPosition;
            }
            
            // Actions
            public bool analyzeFor(Case flag)
            {
                return hasState(m_agent.getPosition(), flag);
            }
            public Agent.MoveOutcome move(Agent.Direction direction)
            {
                if(analyzeFor(Case.Wall))
                    return Agent.MoveOutcome.WallHit;
                if(analyzeFor(Case.Wumpus))
                    return Agent.MoveOutcome.Eaten;
                if(analyzeFor(Case.Well))
                    return Agent.MoveOutcome.Fallen;
                return Agent.MoveOutcome.Moved;
            }
            public bool shootArrow(Agent.Direction direction)
            {
                int offset = 0;
                Case currentTile;
                do
                {
                    Position pos = m_agent.getPosition();
                    if(direction == Agent.Direction.Left || direction == Agent.Direction.Right)
                        pos.x += offset;
                    else
                        pos.y += offset;
                    currentTile = m_map[pos.x, pos.y];
                    if(hasState(pos, Case.Wumpus))
                    {
                        setState(pos, Case.WumpusDead);
                        return true;
                    }

                    if(direction == Agent.Direction.Up || direction == Agent.Direction.Left)
                        --offset;
                    else
                        ++offset;
                } while(currentTile != Case.Wall);

                return false;
            }
            public bool pickGold()
            {
                if(hasState(m_agent.getPosition(), Case.Gold))
                    m_resolved = true;
                return m_resolved;
            }
            
            
            // Flags handling
            private bool hasState(Position position, Case flag)
            {
                if(!checkPosition(position))
                    return false;
                return State.hasState(m_map[position.x, position.y], flag);
            }
            private void setState(Position position, Case flag)
            {
                if(!checkPosition(position))
                    return;
                m_map[position.x, position.y] = flag;
            }
            private void addState(Position position, Case flag)
            {
                if(!checkPosition(position))
                    return;
                m_map[position.x, position.y] = State.addState(m_map[position.x, position.y], flag);
            }
            private void removeState(Position position, Case flag)
            {
                if(!checkPosition(position))
                    return;
                m_map[position.x, position.y] = State.removeState(m_map[position.x, position.y], flag);
            }
            private bool checkPosition(Position position)
            {
                return position.x >= 0 && position.x < m_width
                    && position.y >= 0 && position.y < m_height;
            }        
        }
    }
}