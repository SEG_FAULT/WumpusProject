﻿namespace Wumpus
{
    public abstract partial class Game
    {
        public class State
        {
            public static bool hasState(Case value, Case flag)
            {
                return (value & flag) != 0;
            }
            
            public static bool hasSingleState(Case value, Case flag)
            {
                return (value & flag) == flag;
            }
            
            public static Case addState(Case value, Case flag)
            {
                return (Case)(value | flag);
            }
            
            public static Case removeState(Case value, Case flag)
            {
                return (Case)(value & ~flag); // casting ??
            }
        }
    }
}