﻿using System.Runtime.InteropServices;
using Wumpus.Agents;

namespace Wumpus
{
    public abstract partial class Game
    {
        public abstract class Displayer
        {
            protected Agent m_agent;
            protected Case[,] m_map;
            protected int m_width;
            protected int m_height;

            public void setMapData(Case[,] map, int width, int height)
            {
                m_map = map;
                m_width = width;
                m_height = height;
            }

            public void setAgent(Agent agent)
            {
                m_agent = agent;
            }

            public abstract void display();
            public abstract void update();
            //public abstract void displayStats(Stats stats);
        }
    }
}