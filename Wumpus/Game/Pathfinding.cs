﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Wumpus.Agents;

namespace Wumpus
{
    public class Pathfinding
    {
        public enum CaseType
        {
            Passable = 0,
            Impassable = 1
        }

        private struct Case
        {
            public CaseType type;
            public int value;

            public Case(CaseType t, int v)
            {
                type = t;
                value = v;
            }

            public override string ToString()
            {
                return type + ":" + value;
            }
        }

        private Case[,] m_map;
        private Position m_start;
        private Position m_end;
        private Stack<Agent.Direction> foundPath;

        public Pathfinding()
        {
            foundPath = new Stack<Agent.Direction>();
        }
        public Pathfinding(CaseType[,] map, Position start, Position goal)
        {
            m_start = start;
            m_end = goal;
            int x = map.GetLength(0), y = map.GetLength(1);
            m_map = new Case[x, y];

            for(int i = 0; i < x; ++i)
            {
                for(int j = 0; j < y; ++j)
                    m_map[i, j] = new Case(map[i, j], -1);
            }
            m_map[m_start.x, m_start.y].value = 0;
            foundPath = new Stack<Agent.Direction>();
        }


        private bool updateCase(int x, int y, int value)
        {
            if(x >= m_map.GetLength(0) || y >= m_map.GetLength(1) || x < 0 || y < 0)
                return false;
            if(m_map[x, y].value != -1)
                return false;
            if(m_map[x, y].type == CaseType.Impassable)
                return false;
            m_map[x, y].value = value;
            return true;
        }
        private void propagate(int x, int y, int value)
        {
            List<Position> cases = new List<Position>();
            cases.Add(new Position(x, y));
            updateCase(x, y, value);
            
            while(cases.Count > 0)
            {
                ++value;
                int stepSize = cases.Count;
                for(int i = 0; i < stepSize; ++i)
                {
                    x = cases.ElementAt(0).x;
                    y = cases.ElementAt(0).y;
                    cases.RemoveAt(0);
                    if(updateCase(x + 1, y, value))
                        cases.Add(new Position(x + 1, y));
                    if(updateCase(x - 1, y, value))
                        cases.Add(new Position(x - 1, y));
                    if(updateCase(x, y + 1, value))
                        cases.Add(new Position(x, y + 1));
                    if(updateCase(x, y - 1, value))
                        cases.Add(new Position(x, y - 1));
                }

            }
        }


        private bool nextInPath(int sx, int sy, int nx, int ny)
        {
            if(nx >= m_map.GetLength(0) || ny >= m_map.GetLength(1) || nx < 0 || ny < 0)
                return false;
            if(m_map[nx, ny].value == -1)
                return false;
            if(m_map[nx, ny].type == CaseType.Impassable)
                return false;
            return m_map[nx, ny].value < m_map[sx, sy].value;
        }
        private void backtrack(int x, int y)
        {
            if(nextInPath(x, y, x + 1, y))
            {
                foundPath.Push(Agent.Direction.Left);
                backtrack(x + 1, y);
            }
            else if(nextInPath(x, y, x - 1, y))
            {
                foundPath.Push(Agent.Direction.Right);
                backtrack(x - 1, y);
            }
            else if(nextInPath(x, y, x, y + 1))
            {
                foundPath.Push(Agent.Direction.Down);
                backtrack(x, y + 1);
            }
            else if(nextInPath(x, y, x, y - 1))
            {
                foundPath.Push(Agent.Direction.Up);
                backtrack(x, y - 1);
            }
        }

        public bool hasNextDirection()
        {
            return foundPath.Count > 0;
        }
        public Agent.Direction getNextDirection()
        {
            return foundPath.Pop();
        }
        public void calculatePath()
        {
            propagate(m_start.x, m_start.y, 0);
            backtrack(m_end.x, m_end.y);
            return;
        }
    }
}