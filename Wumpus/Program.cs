﻿using System;

namespace Wumpus
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            bool noDisplay = Properties.Settings.Default.NO_DISPLAY;
            String type = Properties.Settings.Default.TYPE;

            if(type == "tests")
            {
                Testing tests = new Testing();
                tests.randomTests(noDisplay);
            }
            else if(type == "training")
            {
                Training training = new Training();
                training.train();
                //FFNN.tests();
            }
        }
    }
}