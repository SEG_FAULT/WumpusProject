﻿namespace Wumpus
{
    public struct Position
    {
        public int x;
        public int y;
        
        public Position(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public static bool operator==(Position l, Position r)
        {
            return l.x == r.x && r.y == l.y;
        }
        public static bool operator!=(Position l, Position r)
        {
            return !(l == r);
        }

        public override string ToString()
        {
            return "x:" + x + " - y:" + y;
        }
    }
}