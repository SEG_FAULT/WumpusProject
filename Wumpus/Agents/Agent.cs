﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;

namespace Wumpus
{
    namespace Agents
    {
        public abstract class Agent : IAction
        {
            public enum Direction
            {
                Up = 0,
                Right = 1,
                Down = 2,
                Left = 3
            }

            public enum MoveOutcome
            {
                WallHit = 0,
                Fallen,
                Eaten,
                Moved,
                Dead,
                OutOfMovement
            }

            private bool m_alive;
            private bool m_arrow;
            private Position m_position;
            private readonly int m_maxMovement;
            private int m_currentMovementCount;
            
            protected Game.Map m_map;
            protected Direction m_lastSuccessMove;

            private MoveOutcome m_moveOutcome;

            protected Agent(int maxMovement = -1)
            {
                reset();
                m_maxMovement = maxMovement;
            }

            public virtual void reset()
            {
                m_alive = true;
                m_arrow = true;
                m_currentMovementCount = 0;
                m_lastSuccessMove = 0;
                m_moveOutcome = 0;
                m_position = new Position();
            }

            public virtual void setMap(Game.Map map)
            {
                m_position = map.getStartingPosition();
                m_map = map;
            }

            public abstract bool nextMove();

            public int getAge()
            {
                return m_currentMovementCount;
            }

            public int getLife()
            {
                return m_maxMovement - m_currentMovementCount;
            }

            public int getMaxLife()
            {
                return m_maxMovement;
            }

            public bool isAlive()
            {
                return m_alive;
            }

            public MoveOutcome move(Direction direction)
            {
                if(!m_alive)
                    return MoveOutcome.Dead;
                if(m_currentMovementCount < m_maxMovement || m_maxMovement == -1)
                {
                    // Move the agent
                    Position tmp = m_position;
                    /*
                     * Generated map coordinate
                     * y: + is down
                     * x: + is right
                     */
                    if(direction == Direction.Up || direction == Direction.Down)
                        m_position.y += direction == Direction.Up ? -1 : 1;
                    if(direction == Direction.Left || direction == Direction.Right)
                        m_position.x += direction == Direction.Left ? -1 : 1;

                    // Check the outcome of the move
                    m_moveOutcome = m_map.move(direction);
                    if(m_moveOutcome == MoveOutcome.WallHit)
                        m_position = tmp;
                    else if(m_moveOutcome == MoveOutcome.Eaten || m_moveOutcome == MoveOutcome.Fallen)
                        m_alive = false;
                    else if(m_moveOutcome == MoveOutcome.Moved)
                        m_lastSuccessMove = direction;
                    ++m_currentMovementCount;
                    return m_moveOutcome;
                }
                m_alive = false;
                m_moveOutcome = MoveOutcome.OutOfMovement;
                return m_moveOutcome;
            }

            public bool shootArrow(Direction direction)
            {
                if(!m_alive || !m_arrow) return false;
                m_arrow = false;
                return m_map.shootArrow(direction);
            }

            public bool pickGold()
            {
                return m_alive && m_map.pickGold();
            }

            public Position getPosition()
            {
                return m_position;
            }

            public Stats.EndReason getFailReason()
            {
                if(m_map.isResolved())
                    return Stats.EndReason.Success;
                Dictionary<MoveOutcome, Stats.EndReason> dict = new Dictionary<MoveOutcome, Stats.EndReason>();
                dict.Add(MoveOutcome.Eaten, Stats.EndReason.Eaten);
                dict.Add(MoveOutcome.Fallen, Stats.EndReason.Fallen);
                dict.Add(MoveOutcome.Moved, Stats.EndReason.NotEnded);
                dict.Add(MoveOutcome.WallHit, Stats.EndReason.NotEnded);
                dict.Add(MoveOutcome.Dead, Stats.EndReason.OutOfMovement);
                dict.Add(MoveOutcome.OutOfMovement, Stats.EndReason.OutOfMovement);
                return dict.ContainsKey(m_moveOutcome) ? dict[m_moveOutcome] : Stats.EndReason.OutOfMovement;
            }

            public virtual string getSubname()
            {
                return "";
            }
        }
    }
}