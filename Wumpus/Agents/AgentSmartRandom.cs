﻿using System;
using Wumpus.Agents;

namespace Wumpus
{
    public class AgentSmartRandom : AgentDiscover
    {
        private Random m_random;
        private Direction m_prevMove;
        public AgentSmartRandom(int maxMovement = -1) : base(maxMovement)
        {
            m_random = new Random();
        }

        public override bool nextMove()
        {
            if(pickGold())
                return true;
            Direction next;
            if(getLife() > 0 && m_map.analyzeFor(Game.Case.Smell | Game.Case.Wind))
                next = invert(m_prevMove);
            else
                next = (Direction)m_random.Next(0, 4);
            move(next);
            m_prevMove = next;
            return true;
        }

        private Direction invert(Direction d)
        {
            return (Direction)(((int)d + 2) % 4);
        }
    }
}