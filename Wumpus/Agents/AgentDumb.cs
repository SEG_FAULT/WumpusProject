﻿namespace Wumpus
{
    namespace Agents
    {
        public class AgentDumb : Agent
        {
            private Direction m_currentDirection;

            public AgentDumb(int maxMovement = -1) : base(maxMovement)
            {
                m_currentDirection = Direction.Up;
            }

            public override bool nextMove()
            {
                if(pickGold())
                    return true;
                if(move((Direction)((int)m_currentDirection % 4)) != MoveOutcome.Moved)
                    ++m_currentDirection;
                return true;
            }
        }
    }
}