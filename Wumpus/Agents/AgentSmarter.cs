﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Wumpus
{
    public class AgentSmarter : AgentDiscover
    {
        private Pathfinding pathfinder;

        public AgentSmarter(int maxMovement) : base(maxMovement)
        {
            m_random = new Random();
            pathfinder = new Pathfinding();
        }
        private Direction invert(Direction d)
        {
            return (Direction)(((int)d + 2) % 4);
        }
        
        public override bool nextMove()
        {
            if(getLife() == getMaxLife())
            {
                move(Direction.Up);
                return true;
            }
            if(pickGold())
                return true;
            if(!pathfinder.hasNextDirection())
            {
                List<Position> possiblePositions = getClosestSafeUnvisitedCasesPosition();
                if(possiblePositions.Count == 0) // If no safe case, switch to the estimated less dangerous
                    possiblePositions = getLessRiskyUnvisitedCasePosition();
                
                Position nextPos = possiblePositions.ElementAt(m_random.Next(0, possiblePositions.Count));
                pathfinder = new Pathfinding(reduceMapForPathfinding(nextPos), m_positionDiscover, nextPos);
                pathfinder.calculatePath();
            }
            move(pathfinder.getNextDirection());
            return true;
        }

        private Pathfinding.CaseType[,] reduceMapForPathfinding(Position goal)
        {
            if(getLife() == getMaxLife())
                move(Direction.Up);
            int width = getMapWidth(), height = getMapHeight();
            Pathfinding.CaseType[,] map = new Pathfinding.CaseType[width, height];
            for(int i = 0; i < width; ++i)
            {
                for(int j = 0; j < height; ++j)
                {
                    if(m_discover.ContainsKey(new Position(i, j)) && m_discover[new Position(i, j)].visitCount == 0 ||
                       !m_discover.ContainsKey(new Position(i, j)))
                        map[i, j] = Pathfinding.CaseType.Impassable;
                    else
                        map[i, j] = Pathfinding.CaseType.Passable;
                }
            }
            map[goal.x, goal.y] = Pathfinding.CaseType.Passable;
            return map;
        }
    }
}