﻿using System;
using Wumpus.Agents;

namespace Wumpus
{
    public class AgentRandom : Agent
    {
        private Random m_random;
        public AgentRandom(int maxMovement = -1) : base(maxMovement)
        {
            m_random = new Random();
        }

        public override bool nextMove()
        {
            if(pickGold())
                return true;
            move((Direction)m_random.Next(0, 4));
            return true;
        }
    }
}