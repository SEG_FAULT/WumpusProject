﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using Wumpus.Agents;

namespace Wumpus
{
    public abstract class AgentDiscover : Agent
    {
        protected Random m_random;
        public Dictionary<Position, DiscoverCase> m_discover;
        protected Position m_positionDiscover;
        protected int wall_v1, wall_v2, wall_h1, wall_h2;
        
        public AgentDiscover(int maxMovement = -1) : base(maxMovement)
        {
            m_random = new Random();
            reset();
        }

        public override void reset()
        {
            base.reset();
            m_positionDiscover = new Position(0, 0);
            m_discover = new Dictionary<Position, DiscoverCase>();
            m_discover[new Position(-1, 0)] = new DiscoverCase(Game.Case.Wall, new Position(-1, 0));
            m_discover[new Position(0, -1)] = new DiscoverCase(Game.Case.Wall, new Position(0, -1));
            wall_v1 = wall_h1 = wall_v2 = wall_h2 = -1;
            updateVirtualMap();
        }

        private void saveCase()
        {
            if(!m_discover.ContainsKey(m_positionDiscover) || m_discover[m_positionDiscover].visitCount == 0)
            {
                Game.Case current = m_map.analyzeFor(Game.Case.Smell) ? Game.Case.Smell : 0;
                current |= m_map.analyzeFor(Game.Case.Wind) ? Game.Case.Wind : 0;
                //current |= m_map.analyzeFor(Game.Case.Gold) ? Game.Case.Gold : 0;
                //current |= m_map.analyzeFor(Game.Case.Wall) ? Game.Case.Wall : 0;

                if(m_discover.ContainsKey(m_positionDiscover))
                    m_discover[m_positionDiscover].types = current;
                else
                    m_discover[m_positionDiscover] = new DiscoverCase(current, m_positionDiscover);
            }
            ++m_discover[m_positionDiscover].visitCount;
        }

        private void saveCase(Game.Case c)
        {
            if(!m_discover.ContainsKey(m_positionDiscover))
                m_discover[m_positionDiscover] = new DiscoverCase(c, m_positionDiscover);
            else
                ++m_discover[m_positionDiscover].visitCount;
        }

        public new MoveOutcome move(Direction direction)
        {
            // first case
            if(getLife() == getMaxLife())
                saveCase();
            
            MoveOutcome result = base.move(direction);
            Position prev = m_positionDiscover;
            /*
             * Discovered map coordinate
             * y: + is up
             * x: + is right
             */
            switch(direction)
            {
                case Direction.Up:    ++m_positionDiscover.y; break;
                case Direction.Right: ++m_positionDiscover.x; break;
                case Direction.Down:  --m_positionDiscover.y; break;
                case Direction.Left:  --m_positionDiscover.x; break;
                default: throw new ArgumentOutOfRangeException();
            }
            if(result == MoveOutcome.WallHit)
                saveCase(Game.Case.Wall);
            else
                saveCase();
            if(result != MoveOutcome.Moved)
                m_positionDiscover = prev;
            updateVirtualMap();
            return result;
        }

        protected int getMapWidth()
        {
            int result = 0;
            foreach(KeyValuePair<Position,DiscoverCase> discoverCase in m_discover)
                if(discoverCase.Value.pos.x > result)
                    result = discoverCase.Value.pos.x;
            return result + 1;
        }
        
        protected int getMapHeight()
        {
            int result = 0;
            foreach(KeyValuePair<Position,DiscoverCase> discoverCase in m_discover)
                if(discoverCase.Value.pos.y > result)
                    result = discoverCase.Value.pos.y;
            return result + 1;
        }

        private void initUnknownCase(Position pos)
        {
            if(!m_discover.ContainsKey(pos))
            {
                if(pos.x == wall_v1 || pos.x == wall_v2 || pos.y == wall_h1 || pos.y == wall_h2)
                    m_discover[pos] = new DiscoverCase(Game.Case.Wall, pos);
                else
                    m_discover[pos] = new DiscoverCase(pos);
            }
        }
        private void initSurrounding()
        {
            initUnknownCase(getPositionDirection(Direction.Up, m_positionDiscover));
            initUnknownCase(getPositionDirection(Direction.Right, m_positionDiscover));
            initUnknownCase(getPositionDirection(Direction.Down, m_positionDiscover));
            initUnknownCase(getPositionDirection(Direction.Left, m_positionDiscover));
        }

        private void updateWalls()
        {
            int width = getMapWidth();
            int height = getMapHeight();
            for(int i = 0; i < width; ++i)
            {
                Position tmp = new Position(i, -1);
                m_discover[tmp] = new DiscoverCase(Game.Case.Wall, tmp);
            }
            for(int i = 0; i < height; ++i)
            {
                Position tmp = new Position(-1, i);
                m_discover[tmp] = new DiscoverCase(Game.Case.Wall, tmp);
            }
        }
        private void updateVirtualMap()
        {
            updateWalls();
            initSurrounding();
            foreach(KeyValuePair<Position, DiscoverCase> discoverCase in m_discover)
            {
                DiscoverCase[] neighbours = {
                    getCaseDirectionPosition(Direction.Up, discoverCase.Key),
                    getCaseDirectionPosition(Direction.Right, discoverCase.Key),
                    getCaseDirectionPosition(Direction.Down, discoverCase.Key),
                    getCaseDirectionPosition(Direction.Left, discoverCase.Key)                    
                };
                discoverCase.Value.wellProb = 0;
                discoverCase.Value.wumpusProb = 0;
                for(int i = 0; i < 4; ++i)
                {
                    if(neighbours[i] == null) continue;
                    if(neighbours[i].visitCount > 0 && neighbours[i].types == Game.Case.Empty)
                    {
                        discoverCase.Value.wellProb = 0;
                        discoverCase.Value.wumpusProb = 0;
                        break;
                    }
                    if(Game.State.hasState(neighbours[i].types, Game.Case.Wind)) ++discoverCase.Value.wellProb;
                    if(Game.State.hasState(neighbours[i].types, Game.Case.Smell)) ++discoverCase.Value.wumpusProb;
                    
                } 
            }
        }

        protected List<Position> getLessRiskyUnvisitedCasePosition()
        {
            int minDanger = -1;
            List<Position> positions = new List<Position>();
            foreach(KeyValuePair<Position, DiscoverCase> discoverCase in m_discover)
            {
                int x1 = discoverCase.Value.pos.x, y1 = discoverCase.Value.pos.y, x2 = m_positionDiscover.x, y2 = m_positionDiscover.y;
                discoverCase.Value.distance = Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
                int currentDanger = discoverCase.Value.wellProb + discoverCase.Value.wumpusProb;
                if(discoverCase.Value.visitCount == 0 
                   && (minDanger == -1 || currentDanger <= minDanger))
                {
                    if(currentDanger < minDanger)
                        positions.Clear();
                    minDanger = currentDanger;
                    positions.Add(discoverCase.Key);
                }
            }
            return positions;
        }
        
        protected List<Position> getClosestSafeUnvisitedCasesPosition()
        {
            List<Position> positions = new List<Position>();
            double min = -1;
            foreach(KeyValuePair<Position, DiscoverCase> discoverCase in m_discover)
            {
                int x1 = discoverCase.Value.pos.x, y1 = discoverCase.Value.pos.y, x2 = m_positionDiscover.x, y2 = m_positionDiscover.y;
                discoverCase.Value.distance = Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
                if(discoverCase.Value.visitCount == 0 
                   && discoverCase.Value.wellProb == 0 && discoverCase.Value.wumpusProb == 0
                   && (min == -1 || discoverCase.Value.distance <= min))
                {
                    if(discoverCase.Value.distance < min)
                        positions.Clear();
                    min = discoverCase.Value.distance;
                    positions.Add(discoverCase.Key);
                }
            }
            return positions;
        }

        protected int getVisitedCount(Position pos)
        {
            if(!m_discover.ContainsKey(pos))
                return 0;
            return m_discover[pos].visitCount;
        }

        protected Position getPositionDirection(Direction direction, Position center)
        {
            int x = 0, y = 0;
            if(direction == Direction.Left) x = -1;
            else if(direction == Direction.Right) x = 1;
            if(direction == Direction.Down) y = -1;
            else if(direction == Direction.Up) y = 1;
            return new Position(center.x + x, center.y + y);
        }

        protected DiscoverCase getCaseDirection(Direction direction)
        {
            Position pos = m_positionDiscover;
            return getCaseDirectionPosition(direction, pos);
        }
        protected DiscoverCase getCaseDirectionPosition(Direction direction, Position center)
        {
            Position pos = getPositionDirection(direction, center);
            DiscoverCase result;
            m_discover.TryGetValue(pos, out result);
            return result;
        }
        protected Direction invert(Direction d)
        {
            return (Direction)(((int)d + 2) % 4);
        }

        public class DiscoverCase
        {
            public int visitCount;
            public Game.Case types;
            public int wellProb;
            public int wumpusProb;
            public Position pos;
            public double distance;

            public DiscoverCase(Game.Case v, Position p)
            {
                visitCount = v == Game.Case.Wall ? int.MaxValue : 0;
                types = v;
                wellProb = 0;
                wumpusProb = 0;
                pos = p;
            }

            public DiscoverCase(Position p)
            {
                visitCount = 0;
                types = 0;
                wellProb = 0;
                wumpusProb = 0;
                pos = p;
            }

            public bool isEmpty()
            {
                return types == 0;
            }

            public override string ToString()
            {
                return types.ToString() + " (Well:" + wellProb + ", Wumpus:" + wumpusProb + ")";
            }
        }
    }
}