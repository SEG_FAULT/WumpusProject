﻿using System;
using System.Collections.Generic;
using System.Linq;
using Wumpus.NeuralNet;

namespace Wumpus
{
    public class AgentFFNN : AgentDiscover
    {
        private FFNN m_brain;
        public AgentFFNN(FFNN brain, int maxMovement = -1) : base(maxMovement)
        {
            m_brain = brain;
        }

        public int xc = 61;
        public void debug(Direction d, int l, bool current = false)
        {
            Console.SetCursorPosition(xc, l);
            DiscoverCase c = current ? (m_discover.ContainsKey(m_positionDiscover) ? m_discover[m_positionDiscover] : null) : getCaseDirection(d);
            if(current)
                Console.Write("Current\t");
            else
                Console.Write(d + "\t");
            if(c == null)
                Console.Write("Unknown");
            else if(c.isEmpty())
                Console.Write("Empty");
            else
                Console.Write(c.types);
        }

        public void debugLastMovement(int l)
        {
            Console.SetCursorPosition(xc, l);
            Console.Write("Last move\t" + m_lastSuccessMove);
        }

        public void debugOut(List<double> a, int l)
        {
            for(int i = 0; i < a.Count; ++i)
            {
                Console.SetCursorPosition(xc + 20, l + i);
                Console.Write(a[i]);
            }
        }

        public void initDebug()
        {
            for(int i = 0; i < 7; ++i)
            {
                Console.SetCursorPosition(xc, 10 + i);
                Console.Write("                                       ");
            }
        }

        public override bool nextMove()
        {
            if(pickGold())
                return true;
            DiscoverCase currentCase = m_discover.ContainsKey(m_positionDiscover) ? m_discover[m_positionDiscover] : null;
            List<double> inputs = new List<double>();
            inputs.AddRange(getCaseAsInputs(getCaseDirection(Direction.Up)));
            inputs.AddRange(getCaseAsInputs(getCaseDirection(Direction.Right)));
            inputs.AddRange(getCaseAsInputs(getCaseDirection(Direction.Down)));
            inputs.AddRange(getCaseAsInputs(getCaseDirection(Direction.Left)));
            inputs.AddRange(getCaseAsInputs(currentCase));
            inputs.AddRange(getLastMovementNeurons());
            /*Console.Write(getLife() + " :\t");
            foreach(double d in inputs)
                Console.Write(d);
            Console.Write('\t');*/

            
            
            List<double> actions = m_brain.activate(inputs);

            /*
            initDebug();
            debug(Direction.Up, 10);
            debug(Direction.Right, 11);
            debug(Direction.Down, 12);
            debug(Direction.Left, 13);
            debug(Direction.Up, 14, true);
            debugLastMovement(15);
            debugOut(actions, 10);
            //*/
            bool a = takeAction(actions);
            return a;
        }


        private bool takeAction(List<double> outputs)
        {
            int max = outputs.IndexOf(outputs.Max());
            Direction direction = (Direction)max;
            //Console.WriteLine(direction);
            return move(direction) == MoveOutcome.Moved;
        }

        private int low = -1;
        private int high = 0;
        private List<double> getCaseAsInputs(DiscoverCase value)
        {
            List<double> result = new List<double>();
            result.Add(value == null ? high : low);                                                      // Unknow 
            result.Add(value != null && value.isEmpty() ? high : low);                                   // Empty  
            result.Add(value != null && Game.State.hasState(value.types, Game.Case.Wall) ? high : low);  // Wall 
            result.Add(value != null && Game.State.hasState(value.types, Game.Case.Wind) ? high : low);  // Wind 
            result.Add(value != null && Game.State.hasState(value.types, Game.Case.Smell) ? high : low); // Smell   
            return result;
        }

        List<double> getLastMovementNeurons()
        {
            List<double> result = new List<double>();
            result.Add(m_lastSuccessMove == Direction.Up ? high : low);
            result.Add(m_lastSuccessMove == Direction.Right ? high : low);
            result.Add(m_lastSuccessMove == Direction.Down ? high : low);
            result.Add(m_lastSuccessMove == Direction.Left ? high : low);
            return result;
        }

        public override string getSubname()
        {
            string result = "-";
            foreach(int i in m_brain.getNetSchema())
                result += i + ".";
            return result;
        }
    }
}