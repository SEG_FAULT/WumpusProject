﻿using System;
using System.Collections.Generic;
using Wumpus.Agents;

namespace Wumpus
{
    public class Stats
    {
        public enum EndReason { Total, Eaten, Fallen, OutOfMovement, Success, NotEnded }
        
        private readonly SortedDictionary<string, SortedDictionary<string, SortedDictionary<EndReason, int>>> m_statsDictionary;

        public Stats()
        {
            m_statsDictionary = new SortedDictionary<string, SortedDictionary<string, SortedDictionary<EndReason, int>>>();
        }
        
        public void add(Game.Map map, Agent agent)
        {
            EndReason reason;
            if(map.isResolved())
                reason = EndReason.Success;
            else
                reason = agent.getFailReason();
            string agentName = agent.GetType().Name + agent.getSubname();

            addElement(map.getName(), agentName, reason);
        }

        private void addElement(string mapName, string agentName, EndReason reason)
        {
            if(!m_statsDictionary.ContainsKey(mapName))
                m_statsDictionary[mapName] = new SortedDictionary<string, SortedDictionary<EndReason, int>>();
            if(!m_statsDictionary[mapName].ContainsKey(agentName))
                m_statsDictionary[mapName][agentName] = new SortedDictionary<EndReason, int>();
            if(!m_statsDictionary[mapName][agentName].ContainsKey(reason))
                m_statsDictionary[mapName][agentName][reason] = 0;
            if(!m_statsDictionary[mapName][agentName].ContainsKey(EndReason.Total))
                m_statsDictionary[mapName][agentName][EndReason.Total] = 0;
            
            ++m_statsDictionary[mapName][agentName][reason];
            ++m_statsDictionary[mapName][agentName][EndReason.Total];
        }

        public void display()
        {
            Console.WriteLine("================ Stats ================");
            foreach(KeyValuePair<string, SortedDictionary<string, SortedDictionary<EndReason, int>>> maps in m_statsDictionary)
            {
                Console.WriteLine("======== {0} ========", maps.Key);
                foreach(KeyValuePair<string, SortedDictionary<EndReason, int>> reasons in maps.Value)
                {
                    Console.WriteLine("==== Agent '{0}'", reasons.Key);
                    foreach(KeyValuePair<EndReason, int> reason in reasons.Value)
                        if(reason.Key != EndReason.Total)
                        {
                            if(reason.Key == EndReason.Success)
                                Console.ForegroundColor = ConsoleColor.DarkBlue;
                            Console.WriteLine(
                                String.Format("{0} : {1} ({2:0.000}%)",
                                        reason.Key, reason.Value,
                                        reason.Value / (double)reasons.Value[EndReason.Total] * 100.0)
                                    .PadRight(Console.BufferWidth - 1));
                            Console.ResetColor();
                        }
                    Console.WriteLine("".PadRight(Console.BufferWidth - 1));
                }
                Console.WriteLine("".PadRight(Console.BufferWidth - 1));
            }
            Console.WriteLine("".PadRight(Console.BufferWidth - 1));
        }
    }
}