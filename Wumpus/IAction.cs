﻿using Wumpus.Agents;

namespace Wumpus
{
    public interface IAction
    {
        Agent.MoveOutcome move(Agent.Direction direction);
        bool shootArrow(Agent.Direction direction);
        bool pickGold();
    }
}