﻿using System;
using System.Collections.Generic;
using System.Threading;
using Wumpus.Agents;
using Wumpus.NeuralNet;

namespace Wumpus
{
    public class Testing
    {
        private const int displayDelay = 100;
        private bool m_noDisplay;

        private readonly Stats m_stats = new Stats();

        public void randomTests(bool noDisplay, int numberOfRun = -1)
        {
            m_noDisplay = noDisplay;
            List<int[]> maps = new List<int[]>
            {
                //new[] {10, 10, 0},
                //new[] {20, 20, 5},
                //new[] {20, 20, 20},
                new[] {20, 20, 40},
                //new[] {40, 20, 15},
                //new[] {80, 35, 20},
                //new[] {80, 35, 100},
            };
            int epoch = 0;
            while(epoch < numberOfRun || numberOfRun == -1)
            {
                foreach(int[] ints in maps)
                {
                    int mapWidth = ints[0];
                    int mapHeight = ints[1];
                    int wellCount = ints[2];
                    int life = mapWidth * mapHeight * 2;
                    Game.Map map = Game.Map.generateMap(mapHeight, mapWidth, wellCount);

                    Game.Displayer displayer = new Game.ConsoleDisplayer();

                    /*
                     * Agent to test
                     */
                    Agent[] agentToTest =
                    {
                        //new AgentDumb(life),
                        //new AgentRandom(life),
                        new AgentSmarter(10000000),
                        //new AgentSmartRandom(life),
                        //new AgentFFNN(new FFNN(new List<int> {29, 4}), life), 
                        //new AgentFFNN(new FFNN(new List<int> {29, 300, 300, 4}), life), 
                    };

                    foreach(Agent agent in agentToTest)
                        runOneAgent(map, displayer, agent);
                }

                int epochDisplay = 10;
                int epochSubdisplay = epochDisplay / 10;
                ++epoch;
                if(!noDisplay || epoch % epochSubdisplay == 0)
                    Console.Write(".");
                if(!noDisplay || epoch % epochDisplay == 0)
                {
                    Console.WriteLine();
                    Console.WriteLine("Epoch: {0}", epoch);
                    m_stats.display();
                }
            }
        }

        public void tests(List<Agent> agents, List<Game.Map> maps)
        {
            Game.Displayer displayer = new Game.ConsoleDisplayer();

            foreach(Game.Map map in maps)
                foreach(Agent agent in agents)
                    runOneAgent(map, displayer, agent);
        }

        private void runOneAgent(Game.Map map, Game.Displayer displayer, Agent agent)
        {
            agent.reset();
            agent.setMap(map);
            map.reset();
            map.setAgent(agent);
            map.setDisplayer(displayer);

            if(!m_noDisplay)
            {
                displayer.display();
                // Console.WriteLine("Current : {0} ; Life : {1}", agent.GetType().Name, agent.getLife());
                Thread.Sleep(displayDelay);
            }

            while(!map.isResolved() && agent.isAlive())
            {
                bool moved = agent.nextMove();
                if(m_noDisplay) continue;
                if(agent.isAlive())
                {
                    if(moved)
                    {
                        displayer.update();
                        //displayer.display();
                        Console.WriteLine("Current : {0} ; Life : {1}    ", agent.GetType().Name + agent.getSubname(), agent.getLife());
                    }
                }
                else
                    displayer.display();

                if(moved)
                {
                    if(Environment.GetEnvironmentVariable("WAIT_KEY") == "true")
                        Console.ReadKey();
                    else
                        Thread.Sleep(displayDelay);
                }
            }
            if(!m_noDisplay)
                Thread.Sleep(2000);

            m_stats.add(map, agent);
        }
    }
}