﻿using System;
using System.Collections.Generic;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.Statistics;

namespace Wumpus.NeuralNet
{
    public class FFNN
    {
        private const double LowRandomValue = -1;
        private const double HighRandomValue = 1;
        
        private List<Matrix<double>> m_weights;
        private List<int> m_neuronsPerLayer;

        /*
         * Initialize a Feed Forward Neural Network
         *
         * Params:
         *     neuronsPerLayer: number of units per layer
         */
        public FFNN(List<int> neuronsPerLayer)
        {
            m_neuronsPerLayer = neuronsPerLayer;
            m_weights = new List<Matrix<double>>();
            for(int i = 0; i < neuronsPerLayer.Count - 1; ++i)
                m_weights.Add(Matrix<double>.Build.Random(neuronsPerLayer[i], neuronsPerLayer[i + 1],
                                                          new ContinuousUniform(LowRandomValue, HighRandomValue)));
        }

        /*
         * Activate the neural network
         *
         * Params:
         *     inputs: Value for the first layer.
         *             First layer and inputs size must match.
         *
         * Return:
         *     The output values of the neural network.
         */
        public List<double> activate(List<double> inputs)
        {
            // Convert List to Matrix of shape (1, x)    (one row matrix)
            Matrix<double> layerInputs = Matrix<double>.Build.Dense(1, inputs.Count, (rowID, columnID) => inputs[columnID]);
            foreach(var currentWeights in m_weights)
            {
                Matrix<double> outputs = layerInputs.Multiply(currentWeights);
                layerInputs = outputs.Map(MathNet.Numerics.Trig.Tanh, Zeros.Include); // Activation
            }
            return new List<double>(layerInputs.ToColumnMajorArray()); // Flatten output Matrix to a List
        }

        public List<int> getNetSchema()
        {
            return m_neuronsPerLayer;
        }
        
        public List<double> getDNA()
        {
            List<double> result = new List<double>();
            foreach(Matrix<double> matrix in m_weights)
                result.AddRange(matrix.ToColumnMajorArray());
            return result;
        }

        public void setDNA(List<double> dna)
        {
            m_weights = new List<Matrix<double>>();
            for(int i = 0; i < m_neuronsPerLayer.Count - 1; ++i)
            {
                int row = m_neuronsPerLayer[i];
                int col = m_neuronsPerLayer[i + 1];
                double[] layer = dna.GetRange(0, row * col).ToArray();
                m_weights.Add(Matrix<double>.Build.Dense(row, col, layer));
                dna.RemoveRange(0, row * col);
            }
        }
    }
}