﻿using System;
using System.Collections.Generic;
using Wumpus.NeuralNet;

namespace Wumpus.Genetics
{
    public class DNA
    {
        static Random random = new Random();
        
        public static Individual mutate(Individual toMutate, double percent)
        {
            List<double> dna = toMutate.getBrain().getDNA();
            List<int> netSchema = toMutate.getBrain().getNetSchema();
            List<int> genesToMutate = getGenesID(dna, (int)(dna.Count * percent));
            foreach(int geneID in genesToMutate)
                dna[geneID] *= getRandomDouble(0, 2);
            return new Individual(new FFNN(netSchema));
        }


        private static List<int> getGenesID(List<double> dna, int count)
        {
            List<int> result = new List<int>();
            List<int> numbers = new List<int>();
            for(int i = 0; i < dna.Count; ++i)
                numbers.Add(i);
            for(int i = 0; i < count; ++i)
            {
                int id = getRandomInt(0, numbers.Count);
                result.Add(numbers[id]);
                numbers.RemoveAt(id);
            }

            return result;
        }

        private static double getRandomDouble(double min = 0, double max = 1)
        {
            return random.NextDouble() * (max - min) + min;
        }

        private static int getRandomInt(int min = 0, int max = 100)
        {
            return random.Next(min, max);
        }
    }
}