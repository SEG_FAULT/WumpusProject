﻿using System;
using Wumpus.Agents;
using Wumpus.NeuralNet;

namespace Wumpus.Genetics
{
    public class Individual : IComparable
    {
        private FFNN m_ffnn;
        private AgentDiscover m_agent;
        private int m_score;

        public Individual(FFNN ffnn)
        {
            m_ffnn = ffnn;
            m_score = 0;
        }

        public Agent generateAgent(int life)
        {
            return m_agent = new AgentFFNN(m_ffnn, life);
        }

        public int getScore()
        {
            return m_score;
        }

        public void nextMove()
        {
            if(m_agent.nextMove())
                m_score += 10;
            else
                --m_score;

            Stats.EndReason end = m_agent.getFailReason();
            if(end == Stats.EndReason.Success)
                m_score += m_agent.getMaxLife() * m_agent.getLife();
            else if(end == Stats.EndReason.Eaten || end == Stats.EndReason.Fallen)
                m_score -= m_agent.getLife();
        }

        public FFNN getBrain()
        {
            return m_ffnn;
        }
         

        public int CompareTo(object obj)
        {
            Individual i = (Individual)obj;
            if(i.m_score == m_score)
                return 0;
            return i.m_score > m_score ? 1 : -1;
        }
    }
}