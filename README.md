# Équipe

 - Dumortier Corentin
 - Felix Antoine
 - Mignot Simon
 - Roura Florian


# Modélisation
## La caverne

La caverne est un tableau en 2 dimensions. 
Chaque case est représentée par un octet dont chacun des bits correspond à un état, les états pouvant être superposés.

État possible pour les cases sur un octet :
```
Bit :   7      6      5      4      3      2      1      0
              0x40   0x20   0x10   0x08   0x04   0x02   0x01   0x00
              Start   Or   Wumpus Odeur   Vent   Puit   Mur    Vide
```

## L'agent

L'agent est à tout moment placé sur une case et commence toujours sur la case en bas à gauche.
Il ne connait pas la taille de la caverne.

Il possède 5 capteurs :
 - Analyse de la case actuelle
   - Odeur (bit 4)
   - Vent (bit 3)
   - Lueur (Or) (bit 6)
 - Capteur après une action
   - Choc (déplacement dans un mur)
   - Cri (la flèche tirée a touché le Wumpus)
 - Évenement après un déplacement
   - Wumpus -> mort
   - Puit -> mort

Il peut effectuer différentes actions :
 - Déplacement dans une des 4 cases adjacentes
 - Tirer une flèche dans une des 4 directions
 - Analyser la case actuelle avec un des 3 capteurs
 - Récuperer l'or


## Efficacité

Calcul du chemin le plus efficace pour récuperer l'or, avec et sans tuer le Wumpus.


# Solutions

## Réseau de neurones

{{ Description du réseau de neurones utilisé }}

### Algorithme génétique
### Apprentissage supervisé

Backpropagation à partir de réaction humaine à certaines situations.

